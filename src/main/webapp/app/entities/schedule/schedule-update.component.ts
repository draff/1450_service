import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ISchedule, Schedule } from 'app/shared/model/schedule.model';
import { ScheduleService } from './schedule.service';
import { IWorkers } from 'app/shared/model/workers.model';
import { WorkersService } from 'app/entities/workers/workers.service';

@Component({
  selector: 'jhi-schedule-update',
  templateUrl: './schedule-update.component.html',
})
export class ScheduleUpdateComponent implements OnInit {
  isSaving = false;
  workers: IWorkers[] = [];

  editForm = this.fb.group({
    id: [],
    year: [],
    week: [],
    dayOfWeek: [],
    shiftNr: [],
    worker: [],
  });

  constructor(
    protected scheduleService: ScheduleService,
    protected workersService: WorkersService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ schedule }) => {
      this.updateForm(schedule);

      this.workersService.query().subscribe((res: HttpResponse<IWorkers[]>) => (this.workers = res.body || []));
    });
  }

  updateForm(schedule: ISchedule): void {
    this.editForm.patchValue({
      id: schedule.id,
      year: schedule.year,
      week: schedule.week,
      dayOfWeek: schedule.dayOfWeek,
      shiftNr: schedule.shiftNr,
      worker: schedule.worker,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const schedule = this.createFromForm();
    if (schedule.id !== undefined) {
      this.subscribeToSaveResponse(this.scheduleService.update(schedule));
    } else {
      this.subscribeToSaveResponse(this.scheduleService.create(schedule));
    }
  }

  private createFromForm(): ISchedule {
    return {
      ...new Schedule(),
      id: this.editForm.get(['id'])!.value,
      year: this.editForm.get(['year'])!.value,
      week: this.editForm.get(['week'])!.value,
      dayOfWeek: this.editForm.get(['dayOfWeek'])!.value,
      shiftNr: this.editForm.get(['shiftNr'])!.value,
      worker: this.editForm.get(['worker'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISchedule>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IWorkers): any {
    return item.id;
  }
}
