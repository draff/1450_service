import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ISchedule, Schedule } from 'app/shared/model/schedule.model';
import { ScheduleService } from './schedule.service';
import { ScheduleDeleteDialogComponent } from './schedule-delete-dialog.component';
import { IWorkers } from '../../shared/model/workers.model';
import { WorkersService } from '../workers/workers.service';
import * as moment from 'moment';

@Component({
  selector: 'jhi-schedule',
  templateUrl: './schedule.component.html',
})
export class ScheduleComponent implements OnInit, OnDestroy {
  schedules?: ISchedule[];
  workers: IWorkers[] = [];
  eventSubscriber?: Subscription;

  currentYear: number = new Date().getFullYear();
  currentWeek = +moment().format('W');
  isSaving = false;

  constructor(
    protected scheduleService: ScheduleService,
    protected workersService: WorkersService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.scheduleService.query().subscribe((res: HttpResponse<ISchedule[]>) => (this.schedules = res.body || []));
    this.workersService.query().subscribe((res: HttpResponse<IWorkers[]>) => (this.workers = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInSchedules();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ISchedule): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  getSchedule(dayOfWeek: number, shiftNr: number): ISchedule | undefined {
    return this.schedules?.find(
      s => s.year === this.currentYear && s.week === this.currentWeek && s.dayOfWeek === dayOfWeek && s.shiftNr === shiftNr
    );
  }

  private createSchedule(dayOfWeek: number, shiftNr: number): ISchedule {
    return {
      ...new Schedule(),
      year: this.currentYear,
      week: this.currentWeek,
      dayOfWeek,
      shiftNr,
    };
  }

  save($event: any, dayOfWeek: number, shiftNr: number): void {
    const workerId: number = +$event.target.value;
    this.isSaving = true;
    let schedule = this.getSchedule(dayOfWeek, shiftNr);
    if (!schedule) {
      schedule = this.createSchedule(dayOfWeek, shiftNr);
    }
    schedule.worker = this.workers.find(w => w.id === workerId);
    if (schedule.id !== undefined) {
      this.subscribeToSaveResponse(this.scheduleService.update(schedule));
    } else {
      this.subscribeToSaveResponse(this.scheduleService.create(schedule));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISchedule>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  registerChangeInSchedules(): void {
    this.eventSubscriber = this.eventManager.subscribe('scheduleListModification', () => this.loadAll());
  }

  delete(schedule: ISchedule): void {
    const modalRef = this.modalService.open(ScheduleDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.schedule = schedule;
  }

  decreaseWeek(): void {
    if (this.currentWeek === 1) {
      this.currentYear--;
      this.currentWeek = 52;
    } else {
      this.currentWeek--;
    }
  }

  increaseWeek(): void {
    if (this.currentWeek === 52) {
      this.currentYear++;
      this.currentWeek = 1;
    } else {
      this.currentWeek++;
    }
  }
}
