import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IWorkers } from 'app/shared/model/workers.model';

type EntityResponseType = HttpResponse<IWorkers>;
type EntityArrayResponseType = HttpResponse<IWorkers[]>;

@Injectable({ providedIn: 'root' })
export class WorkersService {
  public resourceUrl = SERVER_API_URL + 'api/workers';

  constructor(protected http: HttpClient) {}

  create(workers: IWorkers): Observable<EntityResponseType> {
    return this.http.post<IWorkers>(this.resourceUrl, workers, { observe: 'response' });
  }

  update(workers: IWorkers): Observable<EntityResponseType> {
    return this.http.put<IWorkers>(this.resourceUrl, workers, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IWorkers>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IWorkers[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
