import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IWorkers } from 'app/shared/model/workers.model';

@Component({
  selector: 'jhi-workers-detail',
  templateUrl: './workers-detail.component.html',
})
export class WorkersDetailComponent implements OnInit {
  workers: IWorkers | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ workers }) => (this.workers = workers));
  }

  previousState(): void {
    window.history.back();
  }
}
