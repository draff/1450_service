import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IWorkers } from 'app/shared/model/workers.model';
import { WorkersService } from './workers.service';

@Component({
  templateUrl: './workers-delete-dialog.component.html',
})
export class WorkersDeleteDialogComponent {
  workers?: IWorkers;

  constructor(protected workersService: WorkersService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.workersService.delete(id).subscribe(() => {
      this.eventManager.broadcast('workersListModification');
      this.activeModal.close();
    });
  }
}
