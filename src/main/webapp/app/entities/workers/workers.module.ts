import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppSharedModule } from 'app/shared/shared.module';
import { WorkersComponent } from './workers.component';
import { WorkersDetailComponent } from './workers-detail.component';
import { WorkersUpdateComponent } from './workers-update.component';
import { WorkersDeleteDialogComponent } from './workers-delete-dialog.component';
import { workersRoute } from './workers.route';

@NgModule({
  imports: [AppSharedModule, RouterModule.forChild(workersRoute)],
  declarations: [WorkersComponent, WorkersDetailComponent, WorkersUpdateComponent, WorkersDeleteDialogComponent],
  entryComponents: [WorkersDeleteDialogComponent],
})
export class AppWorkersModule {}
