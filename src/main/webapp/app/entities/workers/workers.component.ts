import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IWorkers } from 'app/shared/model/workers.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { WorkersService } from './workers.service';
import { WorkersDeleteDialogComponent } from './workers-delete-dialog.component';

@Component({
  selector: 'jhi-workers',
  templateUrl: './workers.component.html',
})
export class WorkersComponent implements OnInit, OnDestroy {
  workers: IWorkers[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected workersService: WorkersService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.workers = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.workersService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<IWorkers[]>) => this.paginateWorkers(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.workers = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInWorkers();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IWorkers): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInWorkers(): void {
    this.eventSubscriber = this.eventManager.subscribe('workersListModification', () => this.reset());
  }

  delete(workers: IWorkers): void {
    const modalRef = this.modalService.open(WorkersDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.workers = workers;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateWorkers(data: IWorkers[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.workers.push(data[i]);
      }
    }
  }
}
