import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IWorkers, Workers } from 'app/shared/model/workers.model';
import { WorkersService } from './workers.service';
import { WorkersComponent } from './workers.component';
import { WorkersDetailComponent } from './workers-detail.component';
import { WorkersUpdateComponent } from './workers-update.component';

@Injectable({ providedIn: 'root' })
export class WorkersResolve implements Resolve<IWorkers> {
  constructor(private service: WorkersService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IWorkers> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((workers: HttpResponse<Workers>) => {
          if (workers.body) {
            return of(workers.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Workers());
  }
}

export const workersRoute: Routes = [
  {
    path: '',
    component: WorkersComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Workers',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: WorkersDetailComponent,
    resolve: {
      workers: WorkersResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Workers',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: WorkersUpdateComponent,
    resolve: {
      workers: WorkersResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Workers',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: WorkersUpdateComponent,
    resolve: {
      workers: WorkersResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Workers',
    },
    canActivate: [UserRouteAccessService],
  },
];
