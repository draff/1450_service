import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IWorkers, Workers } from 'app/shared/model/workers.model';
import { WorkersService } from './workers.service';

@Component({
  selector: 'jhi-workers-update',
  templateUrl: './workers-update.component.html',
})
export class WorkersUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    first: [],
    last: [],
    addr: [],
  });

  constructor(protected workersService: WorkersService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ workers }) => {
      this.updateForm(workers);
    });
  }

  updateForm(workers: IWorkers): void {
    this.editForm.patchValue({
      id: workers.id,
      first: workers.first,
      last: workers.last,
      addr: workers.addr,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const workers = this.createFromForm();
    if (workers.id !== undefined) {
      this.subscribeToSaveResponse(this.workersService.update(workers));
    } else {
      this.subscribeToSaveResponse(this.workersService.create(workers));
    }
  }

  private createFromForm(): IWorkers {
    return {
      ...new Workers(),
      id: this.editForm.get(['id'])!.value,
      first: this.editForm.get(['first'])!.value,
      last: this.editForm.get(['last'])!.value,
      addr: this.editForm.get(['addr'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWorkers>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
