import { IWorkers } from 'app/shared/model/workers.model';

export interface ISchedule {
  id?: number;
  year?: number;
  week?: number;
  dayOfWeek?: number;
  shiftNr?: number;
  worker?: IWorkers;
}

export class Schedule implements ISchedule {
  constructor(
    public id?: number,
    public year?: number,
    public week?: number,
    public dayOfWeek?: number,
    public shiftNr?: number,
    public worker?: IWorkers
  ) {}
}
