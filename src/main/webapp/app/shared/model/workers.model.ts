import { ISchedule } from 'app/shared/model/schedule.model';

export interface IWorkers {
  id?: number;
  first?: string;
  last?: string;
  addr?: string;
  schedules?: ISchedule[];
}

export class Workers implements IWorkers {
  constructor(public id?: number, public first?: string, public last?: string, public addr?: string, public schedules?: ISchedule[]) {}
}
