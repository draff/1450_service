package at.jku.corona.web.rest;

import at.jku.corona.domain.Workers;
import at.jku.corona.service.WorkersService;
import at.jku.corona.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link at.jku.corona.domain.Workers}.
 */
@RestController
@RequestMapping("/api")
public class WorkersResource {

    private final Logger log = LoggerFactory.getLogger(WorkersResource.class);

    private static final String ENTITY_NAME = "workers";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WorkersService workersService;

    public WorkersResource(WorkersService workersService) {
        this.workersService = workersService;
    }

    /**
     * {@code POST  /workers} : Create a new workers.
     *
     * @param workers the workers to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new workers, or with status {@code 400 (Bad Request)} if the workers has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/workers")
    public ResponseEntity<Workers> createWorkers(@RequestBody Workers workers) throws URISyntaxException {
        log.debug("REST request to save Workers : {}", workers);
        if (workers.getId() != null) {
            throw new BadRequestAlertException("A new workers cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Workers result = workersService.save(workers);
        return ResponseEntity.created(new URI("/api/workers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /workers} : Updates an existing workers.
     *
     * @param workers the workers to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated workers,
     * or with status {@code 400 (Bad Request)} if the workers is not valid,
     * or with status {@code 500 (Internal Server Error)} if the workers couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/workers")
    public ResponseEntity<Workers> updateWorkers(@RequestBody Workers workers) throws URISyntaxException {
        log.debug("REST request to update Workers : {}", workers);
        if (workers.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Workers result = workersService.save(workers);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, workers.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /workers} : get all the workers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of workers in body.
     */
    @GetMapping("/workers")
    public ResponseEntity<List<Workers>> getAllWorkers(Pageable pageable) {
        log.debug("REST request to get a page of Workers");
        Page<Workers> page = workersService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /workers/:id} : get the "id" workers.
     *
     * @param id the id of the workers to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the workers, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/workers/{id}")
    public ResponseEntity<Workers> getWorkers(@PathVariable Long id) {
        log.debug("REST request to get Workers : {}", id);
        Optional<Workers> workers = workersService.findOne(id);
        return ResponseUtil.wrapOrNotFound(workers);
    }

    /**
     * {@code DELETE  /workers/:id} : delete the "id" workers.
     *
     * @param id the id of the workers to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/workers/{id}")
    public ResponseEntity<Void> deleteWorkers(@PathVariable Long id) {
        log.debug("REST request to delete Workers : {}", id);
        workersService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
