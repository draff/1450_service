/**
 * View Models used by Spring MVC REST controllers.
 */
package at.jku.corona.web.rest.vm;
