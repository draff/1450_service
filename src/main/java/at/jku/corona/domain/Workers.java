package at.jku.corona.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Workers.
 */
@Entity
@Table(name = "workers")
public class Workers implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first")
    private String first;

    @Column(name = "last")
    private String last;

    @Column(name = "addr")
    private String addr;

    @OneToMany(mappedBy = "worker")
    private Set<Schedule> schedules = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirst() {
        return first;
    }

    public Workers first(String first) {
        this.first = first;
        return this;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public Workers last(String last) {
        this.last = last;
        return this;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getAddr() {
        return addr;
    }

    public Workers addr(String addr) {
        this.addr = addr;
        return this;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public Set<Schedule> getSchedules() {
        return schedules;
    }

    public Workers schedules(Set<Schedule> schedules) {
        this.schedules = schedules;
        return this;
    }

    public Workers addSchedule(Schedule schedule) {
        this.schedules.add(schedule);
        schedule.setWorker(this);
        return this;
    }

    public Workers removeSchedule(Schedule schedule) {
        this.schedules.remove(schedule);
        schedule.setWorker(null);
        return this;
    }

    public void setSchedules(Set<Schedule> schedules) {
        this.schedules = schedules;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Workers)) {
            return false;
        }
        return id != null && id.equals(((Workers) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Workers{" +
            "id=" + getId() +
            ", first='" + getFirst() + "'" +
            ", last='" + getLast() + "'" +
            ", addr='" + getAddr() + "'" +
            "}";
    }
}
