package at.jku.corona.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Schedule.
 */
@Entity
@Table(name = "schedule")
public class Schedule implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "year")
    private Integer year;

    @Column(name = "week")
    private Integer week;

    @Column(name = "day_of_week")
    private Integer dayOfWeek;

    @Column(name = "shift_nr")
    private Integer shiftNr;

    @ManyToOne
    @JsonIgnoreProperties(value = "schedules", allowSetters = true)
    private Workers worker;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getYear() {
        return year;
    }

    public Schedule year(Integer year) {
        this.year = year;
        return this;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getWeek() {
        return week;
    }

    public Schedule week(Integer week) {
        this.week = week;
        return this;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }

    public Integer getDayOfWeek() {
        return dayOfWeek;
    }

    public Schedule dayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
        return this;
    }

    public void setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Integer getShiftNr() {
        return shiftNr;
    }

    public Schedule shiftNr(Integer shiftNr) {
        this.shiftNr = shiftNr;
        return this;
    }

    public void setShiftNr(Integer shiftNr) {
        this.shiftNr = shiftNr;
    }

    public Workers getWorker() {
        return worker;
    }

    public Schedule worker(Workers workers) {
        this.worker = workers;
        return this;
    }

    public void setWorker(Workers workers) {
        this.worker = workers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Schedule)) {
            return false;
        }
        return id != null && id.equals(((Schedule) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Schedule{" +
            "id=" + getId() +
            ", year=" + getYear() +
            ", week=" + getWeek() +
            ", dayOfWeek=" + getDayOfWeek() +
            ", shiftNr=" + getShiftNr() +
            "}";
    }
}
