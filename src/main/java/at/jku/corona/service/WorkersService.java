package at.jku.corona.service;

import at.jku.corona.domain.Workers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Workers}.
 */
public interface WorkersService {

    /**
     * Save a workers.
     *
     * @param workers the entity to save.
     * @return the persisted entity.
     */
    Workers save(Workers workers);

    /**
     * Get all the workers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Workers> findAll(Pageable pageable);


    /**
     * Get the "id" workers.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Workers> findOne(Long id);

    /**
     * Delete the "id" workers.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
