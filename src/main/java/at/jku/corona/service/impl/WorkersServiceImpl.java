package at.jku.corona.service.impl;

import at.jku.corona.service.WorkersService;
import at.jku.corona.domain.Workers;
import at.jku.corona.repository.WorkersRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Workers}.
 */
@Service
@Transactional
public class WorkersServiceImpl implements WorkersService {

    private final Logger log = LoggerFactory.getLogger(WorkersServiceImpl.class);

    private final WorkersRepository workersRepository;

    public WorkersServiceImpl(WorkersRepository workersRepository) {
        this.workersRepository = workersRepository;
    }

    @Override
    public Workers save(Workers workers) {
        log.debug("Request to save Workers : {}", workers);
        return workersRepository.save(workers);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Workers> findAll(Pageable pageable) {
        log.debug("Request to get all Workers");
        return workersRepository.findAll(pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<Workers> findOne(Long id) {
        log.debug("Request to get Workers : {}", id);
        return workersRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Workers : {}", id);
        workersRepository.deleteById(id);
    }
}
