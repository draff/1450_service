package at.jku.corona.repository;

import at.jku.corona.domain.Workers;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Workers entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WorkersRepository extends JpaRepository<Workers, Long> {
}
