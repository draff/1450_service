import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppTestModule } from '../../../test.module';
import { WorkersDetailComponent } from 'app/entities/workers/workers-detail.component';
import { Workers } from 'app/shared/model/workers.model';

describe('Component Tests', () => {
  describe('Workers Management Detail Component', () => {
    let comp: WorkersDetailComponent;
    let fixture: ComponentFixture<WorkersDetailComponent>;
    const route = ({ data: of({ workers: new Workers(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AppTestModule],
        declarations: [WorkersDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(WorkersDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(WorkersDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load workers on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.workers).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
