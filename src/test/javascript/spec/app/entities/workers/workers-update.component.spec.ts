import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AppTestModule } from '../../../test.module';
import { WorkersUpdateComponent } from 'app/entities/workers/workers-update.component';
import { WorkersService } from 'app/entities/workers/workers.service';
import { Workers } from 'app/shared/model/workers.model';

describe('Component Tests', () => {
  describe('Workers Management Update Component', () => {
    let comp: WorkersUpdateComponent;
    let fixture: ComponentFixture<WorkersUpdateComponent>;
    let service: WorkersService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AppTestModule],
        declarations: [WorkersUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(WorkersUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(WorkersUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(WorkersService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Workers(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Workers();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
