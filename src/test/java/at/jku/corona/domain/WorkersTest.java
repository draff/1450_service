package at.jku.corona.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import at.jku.corona.web.rest.TestUtil;

public class WorkersTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Workers.class);
        Workers workers1 = new Workers();
        workers1.setId(1L);
        Workers workers2 = new Workers();
        workers2.setId(workers1.getId());
        assertThat(workers1).isEqualTo(workers2);
        workers2.setId(2L);
        assertThat(workers1).isNotEqualTo(workers2);
        workers1.setId(null);
        assertThat(workers1).isNotEqualTo(workers2);
    }
}
