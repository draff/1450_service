package at.jku.corona.web.rest;

import at.jku.corona.Application;
import at.jku.corona.domain.Workers;
import at.jku.corona.repository.WorkersRepository;
import at.jku.corona.service.WorkersService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link WorkersResource} REST controller.
 */
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
@WithMockUser
public class WorkersResourceIT {

    private static final String DEFAULT_FIRST = "AAAAAAAAAA";
    private static final String UPDATED_FIRST = "BBBBBBBBBB";

    private static final String DEFAULT_LAST = "AAAAAAAAAA";
    private static final String UPDATED_LAST = "BBBBBBBBBB";

    private static final String DEFAULT_ADDR = "AAAAAAAAAA";
    private static final String UPDATED_ADDR = "BBBBBBBBBB";

    @Autowired
    private WorkersRepository workersRepository;

    @Autowired
    private WorkersService workersService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restWorkersMockMvc;

    private Workers workers;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Workers createEntity(EntityManager em) {
        Workers workers = new Workers()
            .first(DEFAULT_FIRST)
            .last(DEFAULT_LAST)
            .addr(DEFAULT_ADDR);
        return workers;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Workers createUpdatedEntity(EntityManager em) {
        Workers workers = new Workers()
            .first(UPDATED_FIRST)
            .last(UPDATED_LAST)
            .addr(UPDATED_ADDR);
        return workers;
    }

    @BeforeEach
    public void initTest() {
        workers = createEntity(em);
    }

    @Test
    @Transactional
    public void createWorkers() throws Exception {
        int databaseSizeBeforeCreate = workersRepository.findAll().size();
        // Create the Workers
        restWorkersMockMvc.perform(post("/api/workers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(workers)))
            .andExpect(status().isCreated());

        // Validate the Workers in the database
        List<Workers> workersList = workersRepository.findAll();
        assertThat(workersList).hasSize(databaseSizeBeforeCreate + 1);
        Workers testWorkers = workersList.get(workersList.size() - 1);
        assertThat(testWorkers.getFirst()).isEqualTo(DEFAULT_FIRST);
        assertThat(testWorkers.getLast()).isEqualTo(DEFAULT_LAST);
        assertThat(testWorkers.getAddr()).isEqualTo(DEFAULT_ADDR);
    }

    @Test
    @Transactional
    public void createWorkersWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = workersRepository.findAll().size();

        // Create the Workers with an existing ID
        workers.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restWorkersMockMvc.perform(post("/api/workers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(workers)))
            .andExpect(status().isBadRequest());

        // Validate the Workers in the database
        List<Workers> workersList = workersRepository.findAll();
        assertThat(workersList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllWorkers() throws Exception {
        // Initialize the database
        workersRepository.saveAndFlush(workers);

        // Get all the workersList
        restWorkersMockMvc.perform(get("/api/workers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(workers.getId().intValue())))
            .andExpect(jsonPath("$.[*].first").value(hasItem(DEFAULT_FIRST)))
            .andExpect(jsonPath("$.[*].last").value(hasItem(DEFAULT_LAST)))
            .andExpect(jsonPath("$.[*].addr").value(hasItem(DEFAULT_ADDR)));
    }
    
    @Test
    @Transactional
    public void getWorkers() throws Exception {
        // Initialize the database
        workersRepository.saveAndFlush(workers);

        // Get the workers
        restWorkersMockMvc.perform(get("/api/workers/{id}", workers.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(workers.getId().intValue()))
            .andExpect(jsonPath("$.first").value(DEFAULT_FIRST))
            .andExpect(jsonPath("$.last").value(DEFAULT_LAST))
            .andExpect(jsonPath("$.addr").value(DEFAULT_ADDR));
    }
    @Test
    @Transactional
    public void getNonExistingWorkers() throws Exception {
        // Get the workers
        restWorkersMockMvc.perform(get("/api/workers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWorkers() throws Exception {
        // Initialize the database
        workersService.save(workers);

        int databaseSizeBeforeUpdate = workersRepository.findAll().size();

        // Update the workers
        Workers updatedWorkers = workersRepository.findById(workers.getId()).get();
        // Disconnect from session so that the updates on updatedWorkers are not directly saved in db
        em.detach(updatedWorkers);
        updatedWorkers
            .first(UPDATED_FIRST)
            .last(UPDATED_LAST)
            .addr(UPDATED_ADDR);

        restWorkersMockMvc.perform(put("/api/workers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedWorkers)))
            .andExpect(status().isOk());

        // Validate the Workers in the database
        List<Workers> workersList = workersRepository.findAll();
        assertThat(workersList).hasSize(databaseSizeBeforeUpdate);
        Workers testWorkers = workersList.get(workersList.size() - 1);
        assertThat(testWorkers.getFirst()).isEqualTo(UPDATED_FIRST);
        assertThat(testWorkers.getLast()).isEqualTo(UPDATED_LAST);
        assertThat(testWorkers.getAddr()).isEqualTo(UPDATED_ADDR);
    }

    @Test
    @Transactional
    public void updateNonExistingWorkers() throws Exception {
        int databaseSizeBeforeUpdate = workersRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWorkersMockMvc.perform(put("/api/workers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(workers)))
            .andExpect(status().isBadRequest());

        // Validate the Workers in the database
        List<Workers> workersList = workersRepository.findAll();
        assertThat(workersList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteWorkers() throws Exception {
        // Initialize the database
        workersService.save(workers);

        int databaseSizeBeforeDelete = workersRepository.findAll().size();

        // Delete the workers
        restWorkersMockMvc.perform(delete("/api/workers/{id}", workers.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Workers> workersList = workersRepository.findAll();
        assertThat(workersList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
